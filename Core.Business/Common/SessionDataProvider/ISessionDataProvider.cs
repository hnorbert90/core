﻿namespace Core.Business.Common
{
    public interface ISessionDataProvider
    {
        string Username { get; }
        long LoggedUserID { get; }
    }
}
