﻿namespace Core.Persistence.Configurations.Base
{
    using Core.Domain;

    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public abstract class ConfigurationBase<TEntity> : IEntityTypeConfiguration<TEntity> where TEntity : Entity
    {
        public virtual void Configure(EntityTypeBuilder<TEntity> builder)
        {
            builder.HasKey(e => e.Id);
            builder.Property(e => e.Id).IsRequired().ValueGeneratedOnAdd();
            builder.Property(e => e.ModifiedBy);
            builder.Property(e => e.CreatedBy);
            builder.Property(e => e.DeletedBy);
            builder.Property(e => e.IsDeleted);
            builder.Property(e => e.Name);
        }
    }
}