﻿namespace Core.Business.Commands
{
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;

    using Core.Business.Constans;
    using Core.Persistence.Entities;

    using HS.Common.Abstractions.System;
    using HS.Mediator.Common;

    using Microsoft.AspNetCore.Identity;
    using Microsoft.EntityFrameworkCore;

    public class LockoutUserCommandHandler : HandlerBase<LockoutUserCommand, ResponseBase>
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IDateTimeProvider _dateTimeProvider;

        public LockoutUserCommandHandler(UserManager<ApplicationUser> userManager, IDateTimeProvider dateTimeProvider)
        {
            _userManager = userManager;
            _dateTimeProvider = dateTimeProvider;
        }

        public async override Task<ResponseBase> Handle(LockoutUserCommand request, CancellationToken cancellationToken)
        {
            var user = await _userManager.Users.FirstOrDefaultAsync(x => x.EmployeeID == request.EmployeeId, cancellationToken);
            if (user is null)
            {
                return ResponseBase.Error(ServiceError.ENTITY_NOT_FOUND);
            }

            var result = await _userManager.SetLockoutEndDateAsync(user, _dateTimeProvider.MaxValue);

            return result.Succeeded ? ResponseBase.Ok() : ResponseBase.Error(string.Join(",", result.Errors.Select(e => e.Description)));
        }
    }
}
