﻿namespace Core.Domain.Exceptions
{
    using System;
    public class InvalidPhoneNumberException : Exception
    {
        public InvalidPhoneNumberException(string message) : base(message)
        {
        }
    }
}