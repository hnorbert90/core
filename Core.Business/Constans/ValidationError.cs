﻿namespace Core.Business.Constans
{
    public static class ValidationError
    {
        public const string PASSWORD_REQUIRED = nameof(PASSWORD_REQUIRED);
        public const string PASSWORD_MISSMATCH = nameof(PASSWORD_MISSMATCH);
        public const string INCORRECT_PASSWORD = nameof(INCORRECT_PASSWORD);
        public const string NAME_REQUIRED = nameof(NAME_REQUIRED);
        public const string USERNAME_REQUIRED = nameof(USERNAME_REQUIRED);
    }
}
