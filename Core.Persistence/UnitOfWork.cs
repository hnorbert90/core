﻿namespace Core.Persistence
{
    using Core.Domain;

    public class UnitOfWork : HS.Persistence.EntityFramework.UnitOfWork<CoreDbContext>, IUnitOfWork
    {
        public UnitOfWork(CoreDbContext dbContext) :base(dbContext)
        {
        }
    }
}