const { v4 } = require("uuid");
const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const Agent = require("agentkeepalive");

const mode = process.env.NODE_ENV || "development";

module.exports = {
  entry: "./src/index",
  output: {
    path: path.join(__dirname, "/dist"),
    filename: "main.js",
  },
  resolve: {
    extensions: [".ts", ".tsx", ".js"],
  },
  devtool: mode === "development" ? "source-map" : false,
  mode: mode,
  module: {
    rules: [
      {
        test: /\.(ts|js)x?$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
        },
      },
      {
        test: /\.s[ac]ss$/i,
        use: [
          // Creates `style` nodes from JS strings
          "style-loader",
          // Translates CSS into CommonJS
          "css-loader",
          // Compiles Sass to CSS
          "sass-loader",
        ],
      },
      {
        test: /\.(woff|woff2|eot|ttf|otf|svg)$/,
        use: [
          {
            loader: "file-loader",
          },
        ],
      },
      {
        test: /\.(png|jpe?g|gif)$/i,
        use: [
          {
            loader: "file-loader",
          },
        ],
      },
    ],
  },
  devServer: {
    hot: true,
    port: 3000,
    disableHostCheck: true,
    proxy: {
      "/api": {
        target: "http://localhost:5000",
        changeOrigin: true,
        agent: new Agent({
          maxSockets: 100,
          keepAlive: true,
          maxFreeSockets: 10,
          keepAliveMsecs: 1000,
          timeout: 60000,
          freeSocketTimeout: 30000,
        }),
        onProxyRes: (proxyRes) => {
          var key = "www-authenticate";
          proxyRes.headers[key] =
            proxyRes.headers[key] && proxyRes.headers[key].split(",");
        },
      },
    },
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: "./src/index.html",
      nonce: Buffer.from(v4()).toString("base64"),
    }),
  ],
};
