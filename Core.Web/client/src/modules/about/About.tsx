import React, { useState } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import todoActions from "../../redux/actions/todoActions";

const About = (props: any) => {
  const { todos, saveTodo } = props;
  const [todo, setTodo] = useState<string>("");

  if (!todos) return <div />;

  return (
    <div>
      <button onClick={(e) => saveTodo(todo)}>Add</button>
      <input type="text" onChange={(e: any) => setTodo(e.target.value)} />
      {todos.map((x: string, index: number) => (
        <div key={index}>
          <span>{x}</span>
          <br />
        </div>
      ))}
    </div>
  );
};

About.propTypes = {
  todos: PropTypes.array.isRequired,
  saveTodo: PropTypes.func.isRequired,
};

const mapStateToProps = (state: any) => {
  return {
    todos: state.todos,
  };
};

const mapDispatchToProps = {
  saveTodo: todoActions.saveTodo,
};

export default connect(mapStateToProps, mapDispatchToProps)(About);
