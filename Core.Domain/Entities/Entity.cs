﻿namespace Core.Domain
{
    using System;

    using HS.Domain.Common;

    public class Entity : IEntity<long>, ITrackableEntity<long>, ILogicallyDeletable
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public long CreatedBy { get; set; }
        public long ModifiedBy { get; set; }
        public long DeletedBy { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
    }
}