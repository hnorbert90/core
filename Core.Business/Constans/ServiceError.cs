﻿namespace Core.Business.Constans
{
    public static class ServiceError
    {
        #region Entity
        public const string ENTITY_DELETED = nameof(ENTITY_DELETED);
        public const string ENTITY_REFERENCED_CANNOT_DELETE = nameof(ENTITY_REFERENCED_CANNOT_DELETE);
        public const string ENTITY_ALREADY_DELETED = nameof(ENTITY_ALREADY_DELETED);
        public const string ENTITY_NOT_FOUND = nameof(ENTITY_NOT_FOUND);
        #endregion
    }
}
