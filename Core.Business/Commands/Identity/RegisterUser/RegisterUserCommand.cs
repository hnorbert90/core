﻿namespace Core.Business.Commands
{
    using HS.Mediator.Common;

    public class RegisterUserCommand : RequestBase<ResponseBase>
    {
        public long EmployeeId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
    }
}
