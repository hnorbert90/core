import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import translationHU from "./locales/hu/translation.json";

i18n.use(initReactI18next).init({
  resources: {
    hu: {
      translation: translationHU,
    },
  },
  lng: "hu",
  fallbackLng: "hu",
  preload: ["hu"],
});

export default i18n;
