﻿namespace Core.Web.Seed
{
    using System.Threading.Tasks;

    using Core.Persistence;

    using MediatR;

    public class DataInitializer
    {
        private readonly IMediator _mediator;
        private readonly CoreDbContext _dbContext;

        public DataInitializer(IMediator mediator, CoreDbContext dbContext)
        {
            _mediator = mediator;
            _dbContext = dbContext;
        }

        public async Task SeedData()
        {
        }
    }
}
