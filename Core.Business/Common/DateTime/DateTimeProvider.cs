﻿namespace Core.Business.Common
{
    using System;

    using HS.Common.Abstractions.System;

    public class DateTimeProvider : IDateTimeProvider
    {
        public DateTime Now => DateTime.Now;
        public DateTime UtcNow => DateTime.UtcNow;
        public DateTime Today => DateTime.Today;
        public DateTime MaxValue => DateTime.MaxValue;
        public DateTime MinValue => DateTime.MinValue;
    }
}
