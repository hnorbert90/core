﻿namespace Core.Persistence.Entities
{
    using Microsoft.AspNetCore.Identity;

    public class ApplicationUser : IdentityUser
    {
        public long EmployeeID { get; set; }
    }
}