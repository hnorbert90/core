﻿namespace Core.Web.Controllers
{
    using Core.Business.Common;

    using MediatR;

    public abstract class ControllerBase : Microsoft.AspNetCore.Mvc.ControllerBase
    {
        protected ControllerBase(IMediator mediator, ISessionDataProvider sessionDataProvider)
        {
            Mediator = mediator;
            SessionDataProvider = sessionDataProvider;
        }

        protected IMediator Mediator { get; }
        protected ISessionDataProvider SessionDataProvider { get; }
    }
}
