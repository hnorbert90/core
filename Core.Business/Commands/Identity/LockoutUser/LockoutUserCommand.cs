﻿namespace Core.Business.Commands
{

    using HS.Mediator.Common;

    public class LockoutUserCommand : RequestBase<ResponseBase>
    {
        public long EmployeeId { get; set; }
    }
}
