import React from "react";
import { useTranslation } from "react-i18next";
import Layout from "./layout/Layout";

const App = () => {
  const { t } = useTranslation();

  return <Layout />;
};

export default App;
