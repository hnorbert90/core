import React from "react";
import { Grid } from "@material-ui/core";
import Header from "./Header";
import Footer from "./Footer";
import LandingPage from "../modules/landingPage/LandingPage";
import About from "../modules/about/About";
import PageNotFound from "./PageNotFound";

import { Route, Switch } from "react-router-dom";

const Layout = () => {
  return (
    <Grid container>
      <Grid item xs={12}>
        <Header />
      </Grid>
      <Grid item>
        <Switch>
          <Route exact path="/" component={LandingPage} />
          <Route path="/about" component={About} />
          <Route component={PageNotFound} />
        </Switch>
      </Grid>
      <Grid item xs={12}>
        <Footer />
      </Grid>
    </Grid>
  );
};

export default Layout;
