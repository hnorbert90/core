﻿namespace Core.Domain
{
    using System.Text.RegularExpressions;

    using Core.Domain.Exceptions;

    using HS.DataTypes.Common;

    public class PhoneNumber : ValueObject<string, PhoneNumber>
    {
        private readonly Regex _phoneNumberFormat = new Regex("((?:\\+?3|0)6)(?:-|\\()?(\\d{1,2})(?:-|\\))?(\\d{3})-?(\\d{3,4})");
        protected override void Validate()
        {
            if(string.IsNullOrWhiteSpace(Value))
            {
                throw new InvalidPhoneNumberException("Phone number required!");
            }

            if(!_phoneNumberFormat.Match(Value).Success)
            {
                throw new InvalidPhoneNumberException("Invalid phone number format.");
            }
        }
    }
}
