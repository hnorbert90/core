﻿namespace Core.Web.Extensions
{
    using System;
    using System.Linq;
    using System.Reflection;

    using Microsoft.Extensions.DependencyInjection;

    public static class ServiceCollectionExtensions
    {
        public static void ResolveServiceCollection<T>(this IServiceCollection services, Assembly[] assemblies, ServiceLifetime lifetime = ServiceLifetime.Transient)
        {
            services.ResolveServiceCollection(typeof(T), assemblies, lifetime);
        }

        public static void ResolveServiceCollection(this IServiceCollection services, Type serviceType, Assembly[] assemblies, ServiceLifetime lifetime = ServiceLifetime.Transient)
        {

            var typesFromAssemblies = serviceType.IsGenericType ?
                assemblies.SelectMany(a => a.DefinedTypes.Where(x => !x.IsAbstract && x.GetInterfaces().Any(i => i.IsGenericType && i.GetGenericTypeDefinition().Equals(serviceType)))) :
                assemblies.SelectMany(a => a.DefinedTypes.Where(x => x.GetInterfaces().Contains(serviceType)));
            foreach (var implementationType in typesFromAssemblies)
            {
                if (serviceType.IsGenericType)
                {
                    var genericArguments = implementationType.BaseType.GenericTypeArguments;
                    var genericServiceType = serviceType.MakeGenericType(genericArguments);
                    services.Add(new ServiceDescriptor(genericServiceType, implementationType, lifetime));
                }
                else
                {
                    services.Add(new ServiceDescriptor(serviceType, implementationType, lifetime));
                }

            }
        }
    }
}
