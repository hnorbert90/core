import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import i18n from "./i18n";
import { HashRouter as Router } from "react-router-dom";
import { I18nextProvider } from "react-i18next";
import { CssBaseline } from "@material-ui/core";
import { Provider } from "react-redux";

import configureStore from "./redux/configureStore.dev";

const store = configureStore();

ReactDOM.render(
  <Provider store={store}>
    <React.StrictMode>
      <Router>
        <I18nextProvider i18n={i18n}>
          <CssBaseline />
          <App />
        </I18nextProvider>
      </Router>
    </React.StrictMode>
  </Provider>,
  document.getElementById("root")
);

reportWebVitals();
