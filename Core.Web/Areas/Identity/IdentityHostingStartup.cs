﻿using Microsoft.AspNetCore.Hosting;

[assembly: HostingStartup(typeof(Core.Web.Areas.Identity.IdentityHostingStartup))]
namespace Core.Web.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) => {
            });
        }
    }
}