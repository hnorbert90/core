import React from "react";

const PageNotFound = () => {
  return <div style={{ backgroundColor: "red" }}>Page not found!</div>;
};

export default PageNotFound;
