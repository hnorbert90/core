﻿namespace Core.Business.Commands
{
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;

    using Core.Persistence.Entities;

    using HS.Mediator.Common;

    using Microsoft.AspNetCore.Identity;

    public class RegisterUserCommandHandler : HandlerBase<RegisterUserCommand, ResponseBase>
    {
        private readonly UserManager<ApplicationUser> _userManager;

        public RegisterUserCommandHandler(UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
        }

        public async override Task<ResponseBase> Handle(RegisterUserCommand request, CancellationToken cancellationToken)
        {
            var result = await _userManager.CreateAsync(new ApplicationUser
            {
                UserName = request.UserName,
                EmployeeID = request.EmployeeId
            }, request.Password);

            return result.Succeeded ? ResponseBase.Ok() : ResponseBase.Error(string.Join(",", result.Errors.Select(e => e.Description)));
        }
    }
}
