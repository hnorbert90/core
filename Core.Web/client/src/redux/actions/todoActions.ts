import { createTodo } from "../../services/todoService";
import * as types from "./actionTypes";
import { beginApiCall, apiCallError } from "./apiStatusActions";

function saveTodo(todo: string) {
  //eslint-disable-next-line no-unused-vars
  return function (dispatch: any, getState: any) {
    dispatch(beginApiCall());
    return createTodo(todo)
      .then((todo) => {
        dispatch({ type: types.ADD_TODO, todo });
      })
      .catch((error) => {
        dispatch(apiCallError());
        throw error;
      });
  };
}
export default {
  saveTodo,
};
