namespace Core.Web
{
    using Core.Business.Common;
    using Core.Domain;
    using Core.Persistence;
    using Core.Persistence.Entities;
    using Core.Web.Data;
    using Core.Web.Extensions;
    using Core.Web.Seed;

    using HS.Common.Abstractions.System;
    using HS.Mediator.Common;
    using HS.Mediator.Common.PipelineBehaviors;

    using MediatR;

    using Microsoft.AspNetCore.Authentication;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.SpaServices.ReactDevelopmentServer;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Hosting;

    using Serilog;

    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<CoreDbContext>(options =>
                options.UseSqlite(Configuration.GetConnectionString("SQLiteConnection")));

            services.AddDbContext<IdentityDbContext>(options =>
                options.UseSqlite(Configuration.GetConnectionString("IdentityServerConnection")));

            services.AddDefaultIdentity<ApplicationUser>(options =>
            {

                options.SignIn.RequireConfirmedAccount = false;
                options.Password.RequiredLength = 8;
                options.Password.RequireDigit = true;
                options.Password.RequireLowercase = true;
                options.Password.RequireUppercase = true;
                options.Password.RequireNonAlphanumeric = true;
            }
            ).AddEntityFrameworkStores<IdentityDbContext>();

            services.AddIdentityServer()
                .AddApiAuthorization<ApplicationUser, IdentityDbContext>();

            services.AddAuthentication()
                .AddIdentityServerJwt();

            services.AddSingleton(typeof(IDateTimeProvider), typeof(DateTimeProvider));

            services.AddSingleton(x => Log.Logger);

            services.AddScoped(typeof(IUnitOfWork), typeof(UnitOfWork));

            services.ResolveServiceCollection(typeof(IRequestValidator<,>), new[] { typeof(Business.Business).Assembly });
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(ValidationBehavior<,>));
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(ExceptionBehavior<,>));

            services.AddTransient<IHttpContextAccessor, HttpContextAccessor>();
            services.AddTransient<ISessionDataProvider, SessionDataProvider>();

            services.AddControllersWithViews()
                .AddJsonOptions(options => options.JsonSerializerOptions.PropertyNameCaseInsensitive = true);
            services.AddRazorPages();
            services.AddMediatR(typeof(Business.Business).Assembly);

            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/build";
            });

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Microsoft.OpenApi.Models.OpenApiInfo { Title = "My Service", Version = "v1" });
            });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, CoreDbContext CoreDbContext, IMediator mediator, IdentityDbContext identityDbContext)
        {
            var autoMigrationEnabled = Configuration.GetValue<bool>("UseAutomaticMigration");
            if (autoMigrationEnabled)
            {
                CoreDbContext.Database.Migrate();
                identityDbContext.Database.Migrate();
            }

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();

                new DataInitializer(mediator, CoreDbContext).SeedData().Wait();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseSpaStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseIdentityServer();
            app.UseAuthorization();

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Core.API V1");
            });

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller}/{action=Index}/{id?}");
                endpoints.MapRazorPages();
            });

            app.UseSpa(spa =>
            {
                spa.Options.SourcePath = "ClientApp";

                if (env.IsDevelopment())
                {
                    spa.UseReactDevelopmentServer(npmScript: "start");
                }
            });
        }
    }
}
