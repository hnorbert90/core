import * as types from "../actions/actionTypes";
import initialState from "./initialState";

export default function todoReducer(
  state: any = initialState.todos,
  action: any
) {
  switch (action.type) {
    case types.ADD_TODO:
      return [...state, action.todo];
    default:
      return state;
  }
}
