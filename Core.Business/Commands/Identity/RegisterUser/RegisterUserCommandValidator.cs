﻿namespace Core.Business.Commands
{
    using System.Text.RegularExpressions;
    using System.Threading;
    using System.Threading.Tasks;

    using Core.Business.Constans;

    using HS.Mediator.Common;

    public class RegisterUserCommandValidator : RequestValidatorBase<RegisterUserCommand, ResponseBase>
    {
        private readonly Regex _regex = new Regex(@"^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$");

        public override Task<ResponseBase> ValidateAsync(RegisterUserCommand request, CancellationToken cancellationToken = default)
        {
            if (string.IsNullOrWhiteSpace(request.Password))
            {
                return ResponseBase.Error(ValidationError.PASSWORD_REQUIRED);
            }

            if (!_regex.Match(request.Password).Success)
            {

                return ResponseBase.Error(ValidationError.INCORRECT_PASSWORD);
            }

            if (!request.Password.Equals(request.ConfirmPassword))
            {
                return ResponseBase.Error(ValidationError.PASSWORD_MISSMATCH);
            }

            if (string.IsNullOrWhiteSpace(request.UserName))
            {
                return ResponseBase.Error(ValidationError.USERNAME_REQUIRED);
            }

            return ResponseBase.Ok();
        }
    }
}
